# Real Time Political Analytics Dashboard
As we all know, people are very interested to know which political party will win in the elections. In recent years, there has been attempts to gauge the results have been made by sending messages to news channels and this is how the poll result tells who will win.

We have taken a different approach, and used sentiment analysis for predicting the views of people.

Sentiment analysis is the process of determining whether the text piece expresses positive or negative sentiment. It is also known as opinion mining, deriving the opinions or feelings of a writer. It is widely used to determine the expression of the people such as reviews and survey responses, online and social media posts for applications that range from marketing to customer support.

Sentiment Analysis is done by determining the polarity of the given sentence, document, etc. Polarity classifies the text and analyzes the emotion behind each word, whether the word reflects the emotion of hatred, love, anger, etc. Followed by polarity classification, the polarity of the whole document is determined by its polarity score.

This application can be used by political parties to know where they stand and how they can improve.

Using this approach, an analysis of feelings about products manufactured by companies can be done, so that companies can know the views of people and take appropriate steps.

So here is a screenshot of the real time dashboard.

![screencapture-localhost-4200-1498589542636.png](https://bitbucket.org/repo/9p8BEB4/images/1271105942-screencapture-localhost-4200-1498589542636.png)
  
**Total tweets** are the amount of tweets that are read till now. Which also tells the popularity of the political party.

**Tweets per Minute** is the total amount of tweets in a minute, it changes after every minute.

**Pie Charts** show the comparison between positive and negative tweets, for a particular party.

**Tweets** section displays the tweets from both the parties.

**Languages and Technologies Used**

•	Node.js, Angular, Typescript, HTML, Bootstrap and Express.js.

•	Used socket to make this dashboard real time.

•	Used a public streaming API from twitter to get real time tweets.