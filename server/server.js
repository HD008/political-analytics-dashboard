  var http = require('http');
  var path = require('path');

  var socketio = require('socket.io');
  var express = require('express');

  require('events').EventEmitter.defaultMaxListeners = Infinity;

  var router = express();
  var cors = require('cors');

  router.use(cors());

  var server = http.createServer(router);
  var io = socketio.listen(server);

  var Twit = require('twit');
  
  var T = new Twit({
    consumer_key:         '',//please put keys here
    consumer_secret:      '',//please put keys here
    access_token:         '',//please put keys here
    access_token_secret:  '',//please put keys here
    timeout_ms:           600*1000,  // optional HTTP request timeout to apply to all requests. 
  })

  var ml = require('ml-sentiment')();


  var stream = T.stream('statuses/filter', { track: "modi", language: 'en' });

  var stream1 = T.stream('statuses/filter', { track: "congress", language: 'en' });

      io.on('connection', function (socket) {

     
          stream.on('tweet', function (tweet) {

            var document =
              {
                        text: tweet.text,
                        sentiments: ml.classify(tweet.text)
              };
              
          io.sockets.emit('sendData', document);
        })
        
        stream1.on('tweet', function (tweet) {

            var document =
              {
                        text: tweet.text,
                        sentiments: ml.classify(tweet.text)
              };
              
          io.sockets.emit('sendData1', document);
          })

      });

  // } func end


  
  server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){

  });