import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from '../twitdata/twitdata.services';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  providers: [ChatService]
})

export class ChartComponent implements OnInit {
  sentiments = [];
  connection;
  connection2;
  sentiment;
  a:number=10;
  b:number=10;
  a2:number=10;
  b2:number=10;
  modiMinute:number=0;
  congMinute:number=0;
  lastTotalModi:number=0;
  totalCongress:number=0;
  lastTotalCongress:number=0;
  countCongNegative:number=0;
  countCongPositive:number=0;
totalModi:number=0;
  countnegative:number=0;
  countpositive:number=0;
  
   constructor(public chatService:ChatService) {   
         };

    ngOnInit() {
let something:number=0;
      this.connection = this.chatService.getMessages().subscribe(sentiment => {
      console.log("Sentiments are "+sentiment["sentiments"]);
      this.totalModi++;



      if(sentiment["sentiments"]>0)
      { 
        this.a= this.countpositive++;

          console.log("Positive........................"+this.a);
      }
      else if(sentiment["sentiments"]<0)
      {
        this.b=this.countnegative++;
      }
 this.pieChartData = [this.a, this.b];
    })
		console.log(this.totalModi);
		setInterval(() => { this.modiMinute=this.totalModi-this.lastTotalModi ; this.lastTotalModi=this.totalModi;}, 60000);
  
      this.connection2 = this.chatService.getcongressMessages().subscribe(congSentiment => {
      this.totalCongress++;

      if(congSentiment["sentiments"]>0)
      { 
        this.a2= this.countCongPositive++;
      }
      else if(congSentiment["sentiments"]<0)
      {
        this.b2=this.countCongNegative++;
      }

 this.pieChartData2 = [this.a2, this.b2];
    })

       setInterval(() => { this.congMinute=this.totalCongress-this.lastTotalCongress ; this.lastTotalCongress=this.totalCongress;}, 60000); 
  }


  public pieChartType:string = 'pie';
  
  // Pie
  public pieChartLabels:string[] = ['Negative', 'Positive'];
  public pieChartData:number[] = [this.b, this.a];
  public pieChartData2:number[] = [this.b2, this.a2];
 
   public chartColors: Array<any>= [
 { 
      backgroundColor: ['#f44336','#00e676'],
      borderColor: 'rgba(255, 255, 255, 1)',
      pointBackgroundColor: '#fff',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    }

   ]


  public randomizeType():void {
    this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  }
 
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

    ngOnDestroy() {
    this.connection.unsubscribe();
  }

}