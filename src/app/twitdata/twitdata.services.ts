import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
export class ChatService {
  private url = 'http://localhost:3000'; 
  private socket;

   getMessages() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('sendData', (data) => {
		observer.next(data);  
			
      });

      return () => {
        this.socket.disconnect();
      }; 
    })    
    return observable;
  }

   getcongressMessages() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('sendData1', (data) => {
		observer.next(data);  
			
      });

      return () => {
        this.socket.disconnect();
      }; 
    })    
    return observable;
  } 
}