import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitdataComponent } from './twitdata.component';

describe('TwitdataComponent', () => {
  let component: TwitdataComponent;
  let fixture: ComponentFixture<TwitdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwitdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
