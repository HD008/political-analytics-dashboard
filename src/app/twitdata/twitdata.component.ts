import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from './twitdata.services'

@Component({
  selector: 'app-twitdata',
  templateUrl: './twitdata.component.html',
  styleUrls: ['./twitdata.component.css'],
  providers: [ChatService]
})
export class TwitdataComponent implements OnInit, OnDestroy {
  messages;
  messages2;
  connection;
  connection2;
  

   constructor(private chatService:ChatService) {}
  
    ngOnInit() {
    this.connection = this.chatService.getMessages().subscribe(message => {
      this.messages=JSON.stringify(message["text"]);
    })
    this.connection2 = this.chatService.getcongressMessages().subscribe(congMessage => {
      this.messages2=JSON.stringify(congMessage["text"]);
    })
  }  
    ngOnDestroy() {
    this.connection.unsubscribe();
  }

}
