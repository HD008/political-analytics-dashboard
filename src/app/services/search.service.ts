import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class SearchService {
     
      constructor (private http: Http) {}
     

     getHeroes(hero){
      return this.http.get('http://localhost:3000/search/'+hero).map(res=>res.json());

     }
      
}
